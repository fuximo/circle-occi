from django.views.generic import View
from django.http import HttpResponseRedirect


class IndexView(View):

    def get(self, request, *args, **kwargs):
        if "os" in request.session:
            if request.session["os"].session_alive():
                return HttpResponseRedirect("computes")
        return HttpResponseRedirect("login")
