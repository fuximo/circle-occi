from occilib.occilib import OcciSession
from django.views.generic import TemplateView, View
from django.http import HttpResponseRedirect


class AuthorizedView(TemplateView):

    def dispatch(self, request, *args, **kwargs):
        if "os" in self.request.session:
            if self.request.session["os"].session_alive():
                return super(AuthorizedView, self).dispatch(
                    request, *args, **kwargs)
            else:
                del self.request.session["os"]
        return HttpResponseRedirect("/")

    def get_context_data(self, **kwargs):
        context = super(AuthorizedView, self).get_context_data(**kwargs)
        context["username"] = self.request.session["os"].get_user()
        return context


class LoginView(TemplateView):
    template_name = "login.html"

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        os = OcciSession(request.POST["username"], request.POST["password"])
        if os.session_alive():
            request.session["os"] = os
        return HttpResponseRedirect('/')


class LogoutView(View):

    def get(self, request, *args, **kwargs):
        if "os" in request.session:
            del request.session["os"]
        return HttpResponseRedirect("/")
