from django.conf.urls import url
from occiboard.views import (IndexView, LoginView, LogoutView, ComputesView,
                             ComputeView, ActionView)


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^computes/$', ComputesView.as_view(), name='computes'),
    url(r'^computes/(?P<id>\w+)/$', ComputeView.as_view(), name='compute'),
    url(r'^action/$', ActionView.as_view(), name='action'),
]
