import json
import requests
from subprocess import check_output
from occilib.occilibconf import conf


def OcciSession(username, password):
    """ Creates a Occi11Session or Occi12Session object, depending on the
        configurations in the config file. """
    if conf["occi_version"] == "1.2" and conf["auth"]["type"] == "simple":
        return Occi12Session(username, password)
    elif conf["occi_version"] == "1.1" and conf["auth"]["type"] == "basic":
        return Occi11Session(username, password)
    else:
        return None


class Occi11Session():

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.auth = self.session_alive()

    def get_user(self):
        if self.session_alive():
            return self.username
        return ""

    def session_alive(self):
        command = self.prepare_command()
        command.extend(["-a", "list", "-r", "compute"])
        try:
            json.loads(check_output(command).decode('ascii'))
        except:
            return False
        return True

    def prepare_command(self):
        command = ["occi", "-n", "basic",  "-e", conf["occi_api_url"], "-u",
                   self.username, "-p", self.password, "-o", "json"]
        if not conf["ssl"]:
            command.append("-s")
        return command

    def computes(self):
        command = self.prepare_command()
        command.extend(["-a", "list", "-r", "compute"])
        compute_uris = json.loads(check_output(command).decode('ascii'))
        result = {"resources": []}
        for uri in compute_uris:
            id = uri.split("/")[-1]
            result["resources"].append(self.compute(id))
        return result

    def rocci_to_occi(self, cobj):
        for lev1 in cobj["attributes"]["occi"]:
            for lev2 in cobj["attributes"]["occi"][lev1]:
                cobj["attributes"]["occi." + lev1 + "." + lev2] = \
                    cobj["attributes"]["occi"][lev1][lev2]
        return cobj

    def compute(self, id):
        command = self.prepare_command()
        command.extend(
            ["-a", "describe", "-r", "/compute/" + id])
        compute_obj = json.loads(check_output(command).decode('ascii'))[0]
        return self.rocci_to_occi(compute_obj)

    def templates(self):
        command = self.prepare_command()
        command.extend(["-a", "list", "-r", "os_tpl"])
        tpl_uris = json.loads(check_output(command).decode('ascii'))
        templates = []
        for uri in tpl_uris:
            templates.append(self.template(uri.split("#")[-1]))
        return templates

    def template(self, term):
        command = self.prepare_command()
        command.extend(["-a", "describe", "-r", term])
        template = json.loads(check_output(command).decode('ascii'))[0]
        template["fullterm"] = template["scheme"] + template["term"]
        return template

    def invoke_action(self, kind, id, action):
        """ Invokes "action" on the "id" resource of "kind" type. """
        command = self.prepare_command()
        action = json.loads(action)
        command.extend(["-a", "describe", "-r", "/" + kind +
                        "/" + id, "-g", action["action"].split("#")[-1]])
        if "attributes" in action:
            for atr in action["attributes"]:
                command.extend(["-t", atr + "=" + action["attributes"][atr]])
        print(command)
        return check_output(command).decode('ascii')

    def delete_resource(self, kind, id):
        """ Deletes the "id" resource of "kind" type. """
        command = self.prepare_command()
        command.extend(
            ["--action", "delete", "--resource", "/" + kind + "/" + id])
        return check_output(command)

    def create_resource(self, kind, instance):
        """ Creates the "instance" resource of "kind" type. """
        command = self.prepare_command()
        command.extend(
            ["--action", "create", "--resource", kind, "--attribute",
             'occi.core.title=vm', "--mixin", "resource_tpl#small",
             "--mixin", "os_tpl#" + instance['mixins'][0].split("#")[-1]])
        id = str(check_output(command)
                 ).split('/')[-1].split('\\n')[0]
        return '{"id": ' + str(id) + '}'


class Occi12Session():

    def __init__(self, username, password):
        self.session = requests.Session()
        self.session.cookies.clear()
        self.auth = False
        self.headers = {
            "Content-Type": "application/json",
            "Referer": conf["occi_api_url"],
        }
        self.username = ""
        self.login(username, password)

    def get_user(self):
        if self.session_alive():
            return self.username
        return ""

    def session_alive(self):
        if self.auth:
            r = self.session.get(conf["occi_api_url"] + '-/',
                                 verify=conf["ssl"], headers=self.headers)
            if r.status_code is 200:
                return True
        return False

    def set_csrf_header(self, request):
        if "csrftoken" in request.cookies:
            self.headers["X-CSRFToken"] = request.cookies['csrftoken']

    def login(self, username, password):
        if conf["auth"]["type"] == "simple":
            logindata = {"username": username, "password": password}
            r = self.session.get(conf["auth"]["url"], verify=conf["ssl"])
            self.set_csrf_header(r)
            r = self.session.post(conf["auth"]["url"], verify=conf["ssl"],
                                  data=json.dumps(logindata),
                                  headers=self.headers)
            if r.status_code == 200:
                self.auth = True
                self.username = username
            self.set_csrf_header(r)

    def query_interface(self):
        r = self.session.get(conf["occi_api_url"] + "-/", verify=conf["ssl"],
                             headers=self.headers)
        self.set_csrf_header(r)
        return r.json()

    def computes(self):
        r = self.session.get(conf["occi_api_url"] + "compute/",
                             verify=conf["ssl"], headers=self.headers)
        self.set_csrf_header(r)
        return r.json()

    def compute(self, id):
        r = self.session.get(conf["occi_api_url"] + "compute/" + id,
                             verify=conf["ssl"], headers=self.headers)
        self.set_csrf_header(r)
        return r.json()

    def templates(self):
        templates = list()
        mixins = self.query_interface()["mixins"]
        for mixin in mixins:
            if "depends" in mixin:
                if mixin["depends"] == "http://schemas.ogf.org/occi/infras" + \
                        "tructure#os_tpl":
                    mixin["fullterm"] = mixin["scheme"] + mixin["term"]
                    templates.append(mixin)
        return templates

    def template(self, id):
        c = self.compute(id)
        for template in self.templates():
            if template["fullterm"] in c["mixins"]:
                return template
        return {}

    def invoke_action(self, kind, id, action):
        """ Invokes "action" on the "id" resource of "kind" type. """
        url = conf["occi_api_url"] + kind + "/" + id + "/"
        r = self.session.post(url, headers=self.headers,
                              verify=conf["ssl"], data=action)
        self.set_csrf_header(r)
        return r.text

    def delete_resource(self, kind, id):
        """ Deletes the "id" resource of "kind" type. """
        url = conf["occi_api_url"] + kind + "/" + id + "/"
        r = self.session.delete(url, headers=self.headers,
                                verify=conf["ssl"])
        self.set_csrf_header(r)
        return r.text

    def create_resource(self, kind, instance):
        """ Creates the "instance" resource of "kind" type. """
        url = conf["occi_api_url"] + kind + '/1/'
        r = self.session.put(url, headers=self.headers,
                             verify=conf["ssl"], data=instance)
        self.set_csrf_header(r)
        return r.text
