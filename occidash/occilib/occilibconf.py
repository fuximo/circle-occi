""" This file is the configuration file for the occi dashboard. There are
    sample configurations for the 1.2 occi implementation for CIRCLE and for
    the 1.1 occi implementation for the OpenNebula cloud in comments below. """


conf = {
    # CIRCLE
    # "occi_api_url": "https://vm.ik.bme.hu:15766/occi/",
    # "occi_version": "1.2",
    # "auth": {
    #         "type": "simple",
    #         "url": "https://vm.ik.bme.hu:15766/occi/login"
    #  },
    #  "ssl": False,

    # OpenNebula
    # "occi_api_url": "https://87.97.19.174/",
    # "occi_version": "1.1",
    # "auth": {
    #     "type": "basic",
    # },
    # "ssl": False,

    "occi_api_url": "https://vm.ik.bme.hu:15766/occi/",
    "occi_version": "1.2",
    "auth": {
        "type": "simple",
        "url": "https://vm.ik.bme.hu:15766/occi/login"
    },
    "ssl": False,

}
