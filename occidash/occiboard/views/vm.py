import json
from .login import AuthorizedView
from django.http import HttpResponse


class ComputesView(AuthorizedView):
    template_name = "computes.html"

    def get_context_data(self, **kwargs):
        context = super(ComputesView, self).get_context_data(**kwargs)
        context["computes"] = self.request.session[
            "os"].computes()["resources"]
        context["templates"] = self.request.session["os"].templates()
        return context


class ComputeView(AuthorizedView):
    template_name = "compute.html"

    def get_context_data(self, **kwargs):
        context = super(ComputeView, self).get_context_data(**kwargs)
        context["compute"] = self.request.session["os"].compute(kwargs["id"])
        return context


class ActionView(AuthorizedView):

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode("utf-8"))
        if ("action" not in data) or ("kind" not in data) or \
                ("id" not in data):
            return HttpResponse("", status=400)
        r = request.session["os"].invoke_action(
            data["kind"], str(data["id"]), json.dumps(data["action"]))
        return HttpResponse(r)

    def delete(self, request, *args, **kwargs):
        data = json.loads(request.body.decode("utf-8"))
        if ("kind" not in data) or ("id" not in data):
            return HttpResponse("", status=400)
        r = request.session["os"].delete_resource(
            data["kind"], str(data["id"]))
        return HttpResponse(r)

    def put(self, request, *args, **kwargs):
        data = json.loads(request.body.decode("utf-8"))
        if ("kind" not in data) or ("instance" not in data):
            return HttpResponse("", status=400)
        r = request.session["os"].create_resource(
            data["kind"], json.dumps(data["instance"]))
        return HttpResponse(r)
