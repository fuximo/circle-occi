from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.urlresolvers import reverse
from datetime import datetime
from jinja2 import Environment


def environment(**options):
    env = Environment(extensions=['jinja2_time.TimeExtension'], **options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
    })
    return env
