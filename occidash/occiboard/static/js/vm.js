function spinner(){
    $("#vm-status-bar").removeClass().addClass("fa").addClass("fa-spinner").addClass("fa-spin");
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function invoke_action(id, data){
    spinner();
    $.ajax({
        method: "POST",
        url: '/action/',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            action: data,
            kind: "compute",
            id: id
        }),
        success: function(data){
            location.reload();
        }
    });
}


function start(id){
    data = {
        action: "http://schemas.ogf.org/occi/infrastructure/compute/action#start"
    }
    invoke_action(id, data);
}

function wakeup(id){
    start(id);
}

function sleep(id){
    data = {
        action: "http://schemas.ogf.org/occi/infrastructure/compute/action#suspend",
        attributes: {
            method: "suspend"
        }
    }
    invoke_action(id, data);
}

function reboot(id){
    data = {
        action: "http://schemas.ogf.org/occi/infrastructure/compute/action#restart",
        attributes: {
            method: "warm"
        }
    }
    invoke_action(id, data);
}

function reset(id){
    data = {
        action: "http://schemas.ogf.org/occi/infrastructure/compute/action#restart",
        attributes: {
            method: "cold"
        }
    }
    invoke_action(id, data);
}

function shutdown(id){
    data = {
        action: "http://schemas.ogf.org/occi/infrastructure/compute/action#stop",
        attributes: {
            method: "acpioff"
        }
    }
    invoke_action(id, data);
}

function shutoff(id){
    data = {
        action: "http://schemas.ogf.org/occi/infrastructure/compute/action#stop",
        attributes: {
            method: "poweroff"
        }
    }
    invoke_action(id, data);
}

function destroy(id){
    spinner();
    $.ajax({
        method: "DELETE",
        url: '/action/',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            kind: "compute",
            id: id
        }),
        success: function(data){
            location.replace("/");
        }
    });
}

function create(template){
    $('#vmcreatepopup').modal({
        backdrop: 'static',
        keyboard: false
    });
    $("#modal-body").html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" style="width:100%"></div></div>');
    $.ajax({
        method: "PUT",
        url: "/action/",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            instance: {
                mixins: [
                    template
                ]
            },
            kind: "compute"
        }),
        success: function(data){
            d = $.parseJSON(data);
            location.replace("/computes/" + d["id"]);
        }
    });
}
